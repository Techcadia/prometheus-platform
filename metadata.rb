name 'prometheus-platform'
maintainer 'Sam4Mobile'
maintainer_email 'dps.team@s4m.io'
license 'Apache-2.0'
chef_version '>= 12'
description 'Cookbook used to install and configure prometheus'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/s4m-chef-repositories/prometheus-platform'
issues_url 'https://gitlab.com/s4m-chef-repositories/prometheus-platform'
version '1.0.0'

depends 'ark'
supports 'centos', '>= 7.1'
